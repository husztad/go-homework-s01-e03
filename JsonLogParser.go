package logparser

import (
	"bufio"
	"encoding/json"
	"fmt"
	"os"
	"time"
)

type jsonLogLine struct {
	Ts string `json:"ts"`
}

type JsonLogParser struct {
}

func (a *JsonLogParser) TypeName() string {
	return "json"
}

func (a *JsonLogParser) Parse(filename string) ([]LogLine, error) {
	// open the file
	file, err := os.Open(filename) // For read access.
	if err != nil {
		return nil, fmt.Errorf("while opening '%v' for reading: %v", filename, err)
	}
	// close the file
	defer file.Close()

	// create the empty container
	logLines := make([]LogLine, 0)

	// read line-by-line
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lineText := scanner.Text()
		lineParsedTime, err := ParseJsonLine(lineText)
		if err != nil {
			fmt.Println("ERROR:", err)
			continue
		}

		logLines = append(logLines, LogLine{
			TimeStamp: lineParsedTime,
			Text:      lineText,
			Filename:  filename,
		})

	}

	return logLines, nil
}

func ParseJsonLine(line string) (time.Time, error) {

	var jsonLine jsonLogLine

	// parse json
	if err := json.Unmarshal([]byte(line), &jsonLine); err != nil {
		return time.Time{}, fmt.Errorf("while attempting to parse json line '%v': %v", line, err)
	}

	// parse & log errors (for now)
	parsedTime, err := time.Parse("2006-01-02T15:04:05", jsonLine.Ts)

	if err != nil {
		return time.Time{}, fmt.Errorf("while parsing time: %v", err)
	}

	return parsedTime, nil
}
