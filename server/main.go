package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"logparser"
	"net/http"
	"os"
	"strconv"
	"time"

	"github.com/sirupsen/logrus"
)

func createService() chan<- logparser.FindRequest {

	logFileGroups := []logparser.LogFileGroup{
		{"C:/Users/husztad/Go_training/logs/httpd/access.*.log", "access"},
		{"C:/Users/husztad/Go_training/logs/clustercontroller/clustercontroller.log*", "cluster"},
		{"C:/Users/husztad/Go_training/logs/activemqserver/stdout_activemqserver_*.log", "active"},
		{"C:/Users/husztad/Go_training/logs/vizqlserver/nativeapi_vizqlserver*", "json"},
	}

	parsers := []logparser.LogParser{
		&logparser.DummyLogParser{},
		&logparser.AccesLogParser{},
		&logparser.ClusterLogParser{},
		&logparser.ActiveLogParser{},
		&logparser.JsonLogParser{},
	}

	serviceChan := logparser.MakeLogProcessorService(parsers, logFileGroups)

	return serviceChan
}

type FindJsonRequest struct {
	Start string `json:"start"`
	End   string `json:"end"`
}

type Config struct {
	Port string `json:"port"`
	Addr string `json:"addr"`
	Tls  string `json:"tls"`
	Key  string `json:"key"`
	Cert string `json:"cert"`
}

func getDefaultValue(value, def string) string {
	if value == "" {
		return def
	}
	return value
}

func main() {
	portEnv := os.Getenv("PORT")
	addrEnv := os.Getenv("ADDR")
	tlsEnv := os.Getenv("TLS")
	keyEnv := os.Getenv("KEY")
	certEnv := os.Getenv("CERT")

	portDef, err := strconv.Atoi(getDefaultValue(portEnv, "8080"))
	if err != nil {
		fmt.Printf("Malformed environment variable")
		return
	}

	tlsDef, err := strconv.ParseBool(getDefaultValue(tlsEnv, "false"))
	if err != nil {
		fmt.Printf("Malformed environment variable")
		return
	}

	portRef := flag.Int("port", portDef, "Port number")
	addrRef := flag.String("addr", getDefaultValue(addrEnv, "127.0.0.1"), "Address")
	tlsRef := flag.Bool("tls", tlsDef, "Is TLS used")
	keyRef := flag.String("key", getDefaultValue(keyEnv, "key.pem"), "Key file path")
	certRef := flag.String("cert", getDefaultValue(certEnv, "cert.pem"), "Certificate file path")

	flag.Parse()

	serviceChan := createService()
	//start, _ := time.Parse("2006-01-02T15:04:05", "2020-06-11T13:48:00.000")
	//end, _ := time.Parse("2006-01-02T15:04:05", "2020-06-12T13:48:00.000")

	http.HandleFunc("/logs", func(w http.ResponseWriter, r *http.Request) {

		var req FindJsonRequest

		// Try to decode the request body into the struct. If there is an error,
		// respond to the client with the error message and a 400 status code.
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		start, err := time.Parse("2006-01-02T15:04:05", req.Start)
		if err != nil {
			http.Error(w, "Malformed start time", http.StatusBadRequest)
			return
		}

		end, err := time.Parse("2006-01-02T15:04:05", req.End)
		if err != nil {
			http.Error(w, "Malformed end time", http.StatusBadRequest)
			return
		}

		lines, err := logparser.FindWithService(serviceChan, logparser.TimeRange{start, end})

		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		jsonLinesBytes, err := json.Marshal(lines)

		w.Write(jsonLinesBytes)

		//fmt.Fprintf(w, "Hello, %q %v", html.EscapeString(r.URL.Path), lines)
	})

	http.HandleFunc("/config", func(w http.ResponseWriter, r *http.Request) {

		config := Config{
			Port: strconv.Itoa(*portRef),
			Addr: *addrRef,
			Tls:  strconv.FormatBool(*tlsRef),
			Key:  *keyRef,
			Cert: *certRef,
		}

		jsonBytes, _ := json.Marshal(config)

		w.Write(jsonBytes)

		//fmt.Fprintf(w, "Hello, %q %v", html.EscapeString(r.URL.Path), lines)
	})

	logrus.Info("Runnig server")
	if *tlsRef {
		log.Fatal(http.ListenAndServeTLS(*addrRef+":"+strconv.Itoa(*portRef), *certRef, *keyRef, nil))
	} else {
		log.Fatal(http.ListenAndServe(*addrRef+":"+strconv.Itoa(*portRef), nil))
	}
}
