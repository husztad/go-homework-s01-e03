package main

import (
	"fmt"
	"logparser"
	"time"
)

func main() {

	logFileGroups := []logparser.LogFileGroup{
		{"C:/Users/husztad/Go_training/logs/httpd/access.*.log", "access"},
		{"C:/Users/husztad/Go_training/logs/clustercontroller/clustercontroller.log*", "cluster"},
		{"C:/Users/husztad/Go_training/logs/activemqserver/stdout_activemqserver_*.log", "active"},
		{"C:/Users/husztad/Go_training/logs/vizqlserver/nativeapi_vizqlserver*", "json"},
	}

	parsers := []logparser.LogParser{
		&logparser.DummyLogParser{},
		&logparser.AccesLogParser{},
		&logparser.ClusterLogParser{},
		&logparser.ActiveLogParser{},
		&logparser.JsonLogParser{},
	}

	serviceChan := logparser.MakeLogProcessorService(parsers, logFileGroups)

	//lp := logparser.MakeLogProcessor(parsers)
	//
	//lp.Index(logFileGroups)
	//

	start, _ := time.Parse("2006-01-02T15:04:05", "2020-06-11T13:48:00.000")
	end, _ := time.Parse("2006-01-02T15:04:05", "2020-06-12T13:48:00.000")

	lines, err := logparser.FindWithService(serviceChan, logparser.TimeRange{start, end})
	if err != nil {
		panic(err)
	}

	fmt.Println(lines)
}
