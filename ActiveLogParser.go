package logparser

import (
	"bufio"
	"fmt"
	"os"
	"regexp"
	"strings"
	"time"
)

type ActiveLogParser struct {
}

func (a *ActiveLogParser) TypeName() string {
	return "active"
}

func (a *ActiveLogParser) Parse(filename string) ([]LogLine, error) {
	// open the file
	file, err := os.Open(filename) // For read access.
	if err != nil {
		return nil, fmt.Errorf("while opening '%v' for reading: %v", filename, err)
	}
	// close the file
	defer file.Close()

	// create the empty container
	logLines := make([]LogLine, 0)

	// read line-by-line
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lineText := scanner.Text()
		lineParsedTime, err := ParseActiveLine(lineText)
		if err != nil {
			fmt.Println("ERROR:", err)
			continue
		}

		logLines = append(logLines, LogLine{
			TimeStamp: lineParsedTime,
			Text:      lineText,
			Filename:  filename,
		})

	}

	return logLines, nil
}

var activeLogLineRe = regexp.MustCompile(`^(\d+-\d+-\d+ \d+:\d+:\d+,\d+)`)

func ParseActiveLine(line string) (time.Time, error) {

	result := activeLogLineRe.FindSubmatch([]byte(line))

	// if we have errors
	if result == nil {
		return time.Time{}, fmt.Errorf("cannot find the timestamp in the line '%v'", line)
	}

	// parse the time
	r := strings.Replace(string(result[0]), ",", ".", -1)
	parsedTime, err := time.Parse("2006-01-02 15:04:05.000", r)
	if err != nil {
		return time.Time{}, fmt.Errorf("while parsing time: %v", err)
	}

	return parsedTime, nil
}
